var plugins = document.querySelectorAll('tbody tr.danger');
var output = document.getElementById('plugins-check-info');
var query = "";

if (plugins.length > 0) {
  for (var i = 0; i < plugins.length; i++) {
    if (plugins[i].querySelector('.plugindir').innerText !== '') { 
      var cssClasses = plugins[i].className.split(' ');
      
      var name = cssClasses[1].replace('name-', '');
      var path = plugins[i].querySelector('.plugindir').innerText.trim();
      var version_current = plugins[i].querySelector('.versiondb').innerText.trim();
      var version_new = plugins[i].querySelector('.versiondisk').innerText.trim();

      query += "UPDATE config_plugins SET value = " + version_new +" WHERE plugin = '" 
            + name + "' AND name = 'version' AND value = " + version_current + ";\n";
    }
  }

	output.innerHTML += "<textarea style='margin-top: 20px; height: auto;'>" + query.trim() + "</textarea>";
}