# Plugins Check SQL Generator

## IMPORTANT: Configuring for your dev environment
The plugin uses an array to determine which domains it should load the main JS file on. By default it's set to "etamoodle.local", but it can be easily changed to whatever you use by editing the following in manifest.json:

```javascript
	"content_scripts": [
        {
            "matches": ["*://etamoodle.local/admin/index.php"],
            "js": [
                "moodle-plugin-downgrade-sql-gen.js"
            ]
        }
    ]
```

If you have multiple installs of Moodle with different URLs, you can add them to the array in manifest.json. 


## Installing in Chrome
1. Browse to `chrome://extensions/`
2. Toggle "Developer Mode" to on (top right of the page)
3. Click "Load unpacked"
4. Navigate to the extension's directory, and click "OK"

## Installing in Firefox
1. Browse to `about:addons`
2. Click the cog button (to right of "Manage your extensions")
3. Click "Debug Add-ons"
4. Toggle "Enable add-on debugging" to on, if it's not already
5. Click "Load Temporary Add-on..."
6. Navigate to and select `manifest.json` inside extension's directory and click "Open"

## Updating the extension
Since it's a sideloaded extension and not hosted on the official repositories, updating will be done via git. Once you pull in changes, you'll need to navigate to the addon config page for your browser and reload the extension (should be a link or button below the extension's info). You'll also need to update your dev environment's domain in manifest.json.